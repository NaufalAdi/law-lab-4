# Eksplorasi Lab 4: Implementasi CRUD

Dokumentasi Postman: https://documenter.getpostman.com/view/15252033/UVsQtPum

## URL API (LOCALHOST)
```
URL Localhost: http://127.0.0.1:8000/
URL Create and Get All Books (without id): http://127.0.0.1:8000/api/book
URL Get, Update, Delete Book by Id: http://127.0.0.1:8000/api/book/<int:pk>
```