from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import MovieSerializer, MovieCreateUpdateSerializer
from .models import Movie

class Views(APIView):
     def post(self, request):
          serializer = MovieCreateUpdateSerializer(data=request.data)
          if serializer.is_valid():
               serializer.save()
               return Response(
                    {
                         "data": serializer.data
                    },
                    status=status.HTTP_201_CREATED
               )
          else:
               return Response(
                    {
                         "error": serializer.errors
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
     
     def get(self, request, pk=None):
          if pk:
               movie = get_object_or_404(Movie, pk=pk)
               serializer = MovieSerializer(movie)
               return Response(
                    {
                         "data": serializer.data
                    },
                    status=status.HTTP_200_OK
               )
          movies = Movie.objects.all()
          serializer = MovieSerializer(movies, many=True)
          return Response(
               {
                    "data": serializer.data
               },
               status=status.HTTP_200_OK
          )

     def put(self, request, pk=None):
          movie = get_object_or_404(Movie, pk=pk)
          serializer = MovieCreateUpdateSerializer(movie, data=request.data)
          if serializer.is_valid():
               serializer.save()
               return Response(
                    {
                         "data": serializer.data
                    },
                    status=status.HTTP_200_OK
               )
          else:
               return Response(
                    {
                         "error": serializer.errors
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )

     def delete(self, request, pk=None):
          movie = get_object_or_404(Movie, pk=pk)
          movie.delete()
          return Response(
               {
                    "message": "Item Deleted"
               },
               status=status.HTTP_200_OK
          )