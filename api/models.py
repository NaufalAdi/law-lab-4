from django.db import models

class Movie(models.Model):
     title = models.CharField(max_length=127)
     director = models.CharField(max_length=127)
     duration = models.IntegerField()
     genre = models.CharField(max_length=127)

     def __str__(self) -> str:
         return "{} ({})".format(self.title, str(self.price))