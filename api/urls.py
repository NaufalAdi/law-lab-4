from django.urls import path
from .views import Views
app_name = 'api'
urlpatterns = [
     path('movie', Views.as_view(), name='view'),
     path('movie/<int:pk>', Views.as_view(), name='viewWithId')
]