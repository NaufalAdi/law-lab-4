from dataclasses import field
from rest_framework import serializers
from .models import Movie

class MovieCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'

class MovieSerializer(serializers.ModelSerializer):
     class Meta:
          model = Movie
          fields = ('id', 'title', 'director', 'duration', 'genre')